const DataStore = require("nedb");

function initializeDatabase(opt = {}) {
  let db;
  if (opt.filename) {
    db = new DataStore({
      filename: opt.filename,
      autoload: true,
      inMemoryOnly: false
    });
  } else {
    db = new DataStore();
  }

  return db;
}

const database = new DataStore({
  autoload: true,
  inMemoryOnly: true,
  onload() {
    database.remove({});
    database.ensureIndex({ fieldName: "id" });
    database.insert([
      { id: 150, name: "Jamie" },
      { id: 100, name: "Alan", managerId: 150 },
      { id: 400, name: "Steve", managerId: 150 },
      { id: 220, name: "Martin", managerId: 100 },
      { id: 190, name: "David", managerId: 400 }
    ]);
  }
});

module.exports = { initializeDatabase, database };
