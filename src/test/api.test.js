import { bootstrapAPI } from "../api";
import path from "path";

describe("api", () => {
  let api;
  beforeAll(done => {
    const filename = path.resolve(__dirname, "./test.db");
    api = bootstrapAPI({
      filename: filename
    })();
    done();
  });

  it("should return all employees", async done => {
    const res = await api.findAll();
    expect(res.length).toBe(5);
    done();
  });

  it("should return an employee by id", async done => {
    const res = await api.findById(150);
    expect(res.id).toBe(150);
    done();
  });

  it("should return two employees ", async done => {
    const res = await api.findByManagerId(150);
    expect(res.length).toBe(2);
    done();
  });

  it("should return null", async done => {
    const res = await api.findById(0);
    expect(res).toBe(null);
    done();
  });
});
