const db = require("./db");

function bootstrapAPI(opt = {}) {
  const database = db.initializeDatabase(opt);
  return function() {
    return {
      findAll() {
        return new Promise((res, rej) => {
          database.find({}, (err, docs) => {
            if (err) {
              rej(err);
            }
            console.log("findAll:", docs);
            res(docs);
          });
        });
      },
      findById(id) {
        return new Promise((res, rej) => {
          database.findOne({ id: id }, (err, doc) => {
            if (err) {
              rej(err);
            }
            console.log("findById", doc);
            res(doc);
          });
        });
      },
      findByManagerId(managerId) {
        return new Promise((res, rej) => {
          database.find({ managerId: managerId }, (err, docs) => {
            if (err) {
              rej(err);
            }
            res(docs);
          });
        });
      }
    };
  };
}

const api = {
  findAll() {
    return new Promise((res, rej) => {
      db.database.find({}, (err, docs) => {
        if (err) {
          rej(err);
        }
        res(docs);
      });
    });
  },
  findById(id) {
    return new Promise((res, rej) => {
      db.database.findOne({ id: Number.parseInt(id) }, (err, doc) => {
        if (err) {
          rej(err);
        }
        res(doc);
      });
    });
  },
  findByManagerId(managerId) {
    return new Promise((res, rej) => {
      db.database.find(
        { managerId: Number.parseInt(managerId) },
        (err, docs) => {
          if (err) {
            rej(err);
          }
          res(docs);
        }
      );
    });
  }
};

module.exports = { bootstrapAPI, api };
