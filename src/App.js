import React, { Component } from "react";
import { api } from "./api";
import "./App.css";

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: "",
      employeeList: []
    };
  }

  handleChange = async e => {
    this.setState({ query: e.target.value });
  };

  handleKeypress = async e => {
    if (e.key === "Enter") {
      const id = e.target.value;
      this.getEmployee(id);
    }
  };

  handleClick = async () => {
    this.getEmployee(this.state.query);
  };

  handleShowAll = async () => {
    const employees = await api.findAll();
    this.setState({ employeeList: employees });
  };

  getEmployee = async id => {
    console.log("id:", id);
    const employee = await api.findById(id);
    if (employee) {
      const subordinates = await api.findByManagerId(id);
      employee.subordinates = subordinates;
      this.setState({ employeeList: [employee] });
    } else {
      this.setState({ employeeList: [] });
    }
  };

  render() {
    const { employeeList } = this.state;
    return (
      <div className="root-container">
        <div className="wrapper">
          <header>CRM</header>
          <div className="search-bar">
            <input
              type="text"
              placeholder="enter employee id"
              onChange={this.handleChange}
              onKeyPress={this.handleKeypress}
            />
            <button onClick={this.handleClick}>Search</button>
            <button onClick={this.handleShowAll}>Show All</button>
          </div>
          <div className="result-container">
            <ul>
              {employeeList.length === 0
                ? "Empty"
                : employeeList.map(employee => (
                    <li key={employee._id}>
                      <span className="field">{employee.id}</span>
                      <span className="field">{employee.name}</span>
                      {employee.subordinates && (
                        <ul>
                          {employee.subordinates.map(subordinate => (
                            <li key={subordinate.id}>{subordinate.name}</li>
                          ))}
                        </ul>
                      )}
                    </li>
                  ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
